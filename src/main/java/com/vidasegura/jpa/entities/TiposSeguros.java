/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vidasegura.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bratc
 */
@Entity
@Table(name = "tiposSeguros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposSeguros.findAll", query = "SELECT t FROM TiposSeguros t")
    , @NamedQuery(name = "TiposSeguros.findById", query = "SELECT t FROM TiposSeguros t WHERE t.id = :id")
    , @NamedQuery(name = "TiposSeguros.findByTipo", query = "SELECT t FROM TiposSeguros t WHERE t.tipo = :tipo")
    , @NamedQuery(name = "TiposSeguros.findByDescuento", query = "SELECT t FROM TiposSeguros t WHERE t.descuento = :descuento")})
public class TiposSeguros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "tipo")
    private String tipo;
    @Column(name = "descuento")
    private Integer descuento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoSeguro")
    private List<Ventas> ventasList;

    public TiposSeguros() {
    }

    public TiposSeguros(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    @XmlTransient
    public List<Ventas> getVentasList() {
        return ventasList;
    }

    public void setVentasList(List<Ventas> ventasList) {
        this.ventasList = ventasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposSeguros)) {
            return false;
        }
        TiposSeguros other = (TiposSeguros) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.vidasegura.jpa.session.TiposSeguros[ id=" + id + " ]";
    }
    
}
