/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vidasegura.jpa.sessions;

import com.vidasegura.jpa.entities.TiposSeguros;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author bratc
 */
@Stateless
public class TiposSegurosFacade extends AbstractFacade<TiposSeguros> {

    @PersistenceContext(unitName = "VidaSegura_VidaSegura_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TiposSegurosFacade() {
        super(TiposSeguros.class);
    }
    
}
