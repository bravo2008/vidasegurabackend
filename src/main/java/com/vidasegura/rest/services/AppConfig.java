
package com.vidasegura.rest.services;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

/**
 *
 * @author ruberr
 */
@ApplicationPath("api")
public class AppConfig extends ResourceConfig{

    public AppConfig() {
         packages("com.vidasegura.rest.services;com.vidasegura.rest.auth");
        register(RolesAllowedDynamicFeature.class);
    }
}
