
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vidasegura.rest.services;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vidasegura.jpa.entities.Roles;
import com.vidasegura.jpa.entities.Usuarios;
import com.vidasegura.jpa.entities.TiposSeguros;
import com.vidasegura.jpa.sessions.TiposSegurosFacade;
import com.vidasegura.rest.auth.DigestUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author bratc
 */
@Stateless
@Path("tiposSeguros")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TiposSegurosREST {
    

    @EJB
    private TiposSegurosFacade tiposSegurosEJB;
   
    /**
     * Obtiene todos los tiposSeguros
     *
     * @return lista de tiposSeguros
     */
    @GET
   // @TiposSegurosAllowed({"ADMIN"})
    public List<TiposSeguros> findAll() {
        return tiposSegurosEJB.findAll();
    }
    
    @POST  
    public Response create(TiposSeguros tiposSeguro) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        tiposSegurosEJB.create(tiposSeguro);
        return Response.status(Response.Status.CREATED).entity(gson.toJson("El de tiposSeguro  se creó correctamente!")).build();

                
               
    }

     /**
     * Busca tiposSeguros por su id
     *
     * @param id
     * @return tiposSeguros
     */
    @GET
    @Path("{id}")
    public TiposSeguros findById(@PathParam("id") Integer id) {
        return tiposSegurosEJB.find(id);
    }
    
    @PUT
    @Path("{id}")
    public void edit(@PathParam("id") Integer id, TiposSeguros tiposSeguro){
        tiposSegurosEJB.edit(tiposSeguro);
    }
    
}
