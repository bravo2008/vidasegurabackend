
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vidasegura.rest.services;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vidasegura.jpa.entities.Roles;
import com.vidasegura.jpa.entities.Usuarios;
import com.vidasegura.jpa.entities.Ventas;
import com.vidasegura.jpa.sessions.VentasFacade;
import com.vidasegura.rest.auth.DigestUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author bratc
 */
@Stateless
@Path("ventasWeb")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VentasREST {
    

    @EJB
    private VentasFacade ventasEJB;
   
    /**
     * Obtiene todos los ventas
     *
     * @return lista de ventas
     */
    @GET
   // @VentasAllowed({"ADMIN"})
    public List<Ventas> findAll() {
        return ventasEJB.findAll();
    }
    
    @POST  
    public Response create(Ventas venta) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        venta.setFecha(null);
        ventasEJB.create(venta);
        return Response.status(Response.Status.CREATED).entity(gson.toJson("La venta  se creó correctamente!")).build();

                
               
    }

     /**
     * Busca ventas por su id
     *
     * @param id
     * @return ventas
     */
    @GET
    @Path("{id}")
    public Ventas findById(@PathParam("id") Integer id) {
        return ventasEJB.find(id);
    }
    
    @PUT
    @Path("{id}")
    public void edit(@PathParam("id") Integer id, Ventas venta){
        ventasEJB.edit(venta);
    }
    
}
