package com.vidasegura.jpa.entities;

import com.vidasegura.jpa.entities.TiposSeguros;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-19T06:10:55")
@StaticMetamodel(Ventas.class)
public class Ventas_ { 

    public static volatile SingularAttribute<Ventas, String> apellidos;
    public static volatile SingularAttribute<Ventas, Date> fecha;
    public static volatile SingularAttribute<Ventas, BigDecimal> valorConDescuento;
    public static volatile SingularAttribute<Ventas, String> numeroDocumentoCliente;
    public static volatile SingularAttribute<Ventas, BigDecimal> valorSeguro;
    public static volatile SingularAttribute<Ventas, TiposSeguros> idTipoSeguro;
    public static volatile SingularAttribute<Ventas, Integer> id;
    public static volatile SingularAttribute<Ventas, String> telefono;
    public static volatile SingularAttribute<Ventas, String> nombres;

}