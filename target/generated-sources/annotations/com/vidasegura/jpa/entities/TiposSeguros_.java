package com.vidasegura.jpa.entities;

import com.vidasegura.jpa.entities.Ventas;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-19T06:10:55")
@StaticMetamodel(TiposSeguros.class)
public class TiposSeguros_ { 

    public static volatile SingularAttribute<TiposSeguros, String> tipo;
    public static volatile SingularAttribute<TiposSeguros, Integer> descuento;
    public static volatile ListAttribute<TiposSeguros, Ventas> ventasList;
    public static volatile SingularAttribute<TiposSeguros, Integer> id;

}